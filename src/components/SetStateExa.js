import React, { Component } from 'react'

class SetStateExa extends Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 0
        }
    }

    increment(){
        //dont modify value directly.Use setState() method
        //use callback fuction to know updated value of state
        // this.setState({
        //     count: this.state.count +1
        // }, () => console.log('Value', this.state.count))
        // console.log(this.state.count)

        this.setState((prevState, props )=> ({
            count : prevState.count+1
        }), console.log('Value', this.state.count))
    }
    //This is grouped as single function instead giving 5 as output it will give 1 as output so, we use
    incrementFive(){
        this.increment()
        this.increment()
        this.increment()
        this.increment()
        this.increment()
    }
    render() {
        return (
            <div>
                <div>Count = {this.state.count}</div>
                <button onClick={() => this.incrementFive()}>Increment</button>
            </div>
        )
    }
}

export default SetStateExa;
