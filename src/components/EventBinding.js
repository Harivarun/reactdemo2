import React, { Component } from 'react'

class EventBinding extends Component {
    constructor(props){
        super(props)
        this.state ={
            message: 'Hi'
        }
        
        this.clickHandlerthree = this.clickHandlerthree.bind(this)
    }

    clickHandler(){
        this.setState({
            message: 'Using .bind() method'
        })
        console.log(this)
    }

    clickHandlertwo(){
        this.setState({
            message: 'Using Arrow Function'
        })
        console.log(this)
    }
    
    clickHandlerthree(){
        this.setState({
            message: 'binding in class constructor'
        })
        console.log(this)
    }
    
    clickHandlerfour = () =>{
        this.setState({
            message: 'binding using arrow function'
        })
        console.log(this)
    }

    render() {
        return (
            <div>
                <div>{this.state.message}</div>
                {/* Approach one in binding using .bind() method -- performance implication */}
                <button onClick={this.clickHandler.bind(this)}>Approach1</button>
                {/* Approach two in binding using arrow function */}
                <button onClick={() => this.clickHandlertwo()}>Approach2</button>
                {/* Appraoch three in binding i.e, binding in class constructor -- react suggested approach*/}
                <button onClick={this.clickHandlerthree}>Approach3</button>
                {/* Approach four in binding is Arrow function  as class property -- react suggested approach */}
                <button onClick={this.clickHandlerfour}>Appraoch4</button>
            </div>
        )
    }
}

export default EventBinding
