import React, { Component } from 'react'

class EventHandlingTwo extends Component {
    clickHandler(){
        console.log('Class Click');
        
    }
    render() {
        return (
            <div>
                <button onClick={this.clickHandler}>Class Click</button>
            </div>
        )
    }
}

export default EventHandlingTwo
