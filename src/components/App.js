import React from "react";
import "../styles/App.css";
import Stateless from "./Functional";
import Statefull from "./Statefull";
import JsxExa from "./JsxExa";
import PropsExa from "./PropsExa";
import StateExa from "./StateExa";

import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import SetStateExa from "./SetStateExa";
import EventHandling from "./EventHandling";
import EventHandlingTwo from "./EventHandlingTwo";
import EventBinding from "./EventBinding";
import MethodsAsPropsParent from "./MethodsAsPropsParent";
import "../styles/DemoStyles.css";

function App() {
  return (
    <div className="App">
      <div className="App-header">
        <h2>React Demos</h2>
      </div>
      <Tabs>
        <TabList>
          <Tab>Components</Tab>
          <Tab>JSX Examples</Tab>
          <Tab>Props Example</Tab>
          <Tab>State Example</Tab>
          <Tab>Event Handling</Tab>
          <Tab>Event Binding</Tab>
          <Tab>Methods as props</Tab>
        </TabList>

        <TabPanel>
          <Stateless />
          <Statefull />
        </TabPanel>
        <TabPanel>
          <JsxExa />
        </TabPanel>
        <TabPanel>
          <PropsExa name="varun" />
          <PropsExa value="hari" />
        </TabPanel>
        <TabPanel>
          <StateExa message="State Example" />
          <p></p>
          <SetStateExa />
        </TabPanel>
        <TabPanel>
          <EventHandling />
          <p></p>
          <EventHandlingTwo />
          <p></p>
        </TabPanel>
        <TabPanel>
          <EventBinding />
        </TabPanel>
        <TabPanel>
          <MethodsAsPropsParent />
        </TabPanel>
      </Tabs>
    </div>
  );
}

export default App;
