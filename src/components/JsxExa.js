import React, {
    Component
} from 'react';

class JsxExa extends Component {
    render() {
        return (
            React.createElement('div', null, 
                React.createElement('div', null, 'This is JSX example without H1 tag'), 
                    React.createElement('div', null, 
                        React.createElement('h1', null, 'This is JSX Example with H1')
                )
            )
        );
    }
}

export default JsxExa;