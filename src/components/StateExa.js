import React,{Component} from 'react';

class StateExa extends Component{

    constructor(props){
        super(props)
        this.state = {
            message: props.message
        }
    }
    changeText() {
        this.setState({
            message: 'State Example on Button Click'
        })
    }
    render(){
        return(
            <div>
                <h1>{this.state.message}</h1>
                <button onClick={() => this.changeText()}>ChangeState</button>
            </div>
        );
    }
}

export default StateExa;