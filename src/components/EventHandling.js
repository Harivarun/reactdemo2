import React from 'react'

function EventHandling() {

    function clickHandler(){
        console.log('Clicked');
        
    }
    return (
        <div>
            {/* Dont add paranthesis to call function */}
            <button onClick={clickHandler}>Fuctional Click</button>
        </div>
    )
}

export default EventHandling
